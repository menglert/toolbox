import pandas as pd
import glob
import re

path = "./"
#regex = r"9[0-9]{5}"
regex = r""

filenames = glob.glob(path + "/*.xlsx")

data = []

for file in filenames:
   excelDf = pd.read_excel(file, sheet_name=None)
   #print(file)
   for key, value in excelDf.items():
        #print("\t" + key)
        text = value.to_string().split('\t')
        list = re.findall(regex,str(text))
        #print("\t\t" + str(sorted(set(list))))
        for item in sorted(set(list)):
            data.append({'file': file, 'sheet': key, 'result': item})

outputDf = pd.DataFrame(data)
outputDf.to_excel("output.xlsx",sheet_name='summary')
        
